.. title: What We're Reading
.. slug: reading-list
.. date: 2019-06-17
.. description: Our reading list while working on the IASGE project.
.. type: text


This is a lightly curated list from our Zotero group, which is public (with closed membership) and viewable here: https://www.zotero.org/groups/2308144/iasge. You can download this list as BibTex in our `GitLab Repository <https://gitlab.com/investigating-archiving-git/investigating-archiving-git.gitlab.io/blob/master/bibtex/reading.bib>`_.

.. publication_list:: bibtex/reading.bib
   :style: unsrt
   :detail_page_dir: