.. title: Our Scholarly Work
.. slug: scholarly-work
.. date: 2019-10-14
.. description: Scholarly output from our Sloan grant.
.. type: text

.. publication_list:: bibtex/presos.bib
   :style: unsrt
   :detail_page_dir: 
